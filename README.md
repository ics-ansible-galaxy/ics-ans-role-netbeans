# ics-ans-role-netbeans

Ansible role to install [NetBeans IDE](https://netbeans.org).
It also install the JIRA plugin.

## Requirements

- ansible >= 2.7
- molecule >= 2.6

## Role Variables

```yaml
---
netbeans_ide_version: 12
netbeans_archive: https://artifactory.esss.lu.se/artifactory/swi-pkg/netbeans/{{ netbeans_ide_version }}/netbeans-12.0-bin.zip

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-netbeans
```

## License

BSD 2-clause
