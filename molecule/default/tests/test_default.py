import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_netbeans_binary_checksum(host):
    assert (
        host.file("/opt/netbeans/bin/netbeans").md5sum
        == "1cb7221e0f144e37a014e7e79ee9a868"
    )


def test_netbeans_binary_exists(host):
    assert host.file("/opt/netbeans/bin/netbeans").exists


def test_netbeans_binary_help(host):
    cmd = host.command("/opt/netbeans/bin/netbeans --help 2>&1")
    assert "Usage:" in cmd.stdout
    assert "General options:" in cmd.stdout
    assert "Additional module options:" in cmd.stdout


def test_netbeans_jdkhome(host):
    netbeans_conf = host.file("/opt/netbeans/etc/netbeans.conf")
    # We don't check the minor version of the JDK (only 1.8)
    assert netbeans_conf.contains('netbeans_jdkhome="/opt/java/jdk-11.0.8+10')
